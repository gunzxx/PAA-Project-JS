-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Apr 2023 pada 06.30
-- Versi server: 10.4.27-MariaDB
-- Versi PHP: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paa_project`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_enkripsi`
--

CREATE TABLE `data_enkripsi` (
  `id` int(11) NOT NULL,
  `plaintext` varchar(255) NOT NULL,
  `ciphertext` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `data_enkripsi`
--

INSERT INTO `data_enkripsi` (`id`, `plaintext`, `ciphertext`, `user_id`) VALUES
(3, 'coba11', 'Y29iYTE=', 2),
(5, 'tes', 'dGVz', 2),
(11, 'coba', 'Y29iYQ==', 2),
(17, 'saya ingin mendecode', 'c2F5YSBpbmdpbiBtZW5kZWNvZGU=', 2),
(21, 'coba1', 'Y29iYQ==', 2),
(24, 'coba', 'sadkSUG789q=', 3),
(26, 'tambah', 'dGFtYmFo', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nickname`, `username`, `password`) VALUES
(1, 'admin', 'admin', '123'),
(2, 'gunz', 'gunzxx', '123'),
(3, 'undefined', 'undefined', 'undefined'),
(4, 'guntur', 'guntur', '1234'),
(5, 'Gunz', 'gunzxx1', '123'),
(6, 'gunz3', 'gunzxx3', '123'),
(7, 'anjayy', 'anjayy', 'anjayy'),
(8, 'guntur', 'guntur234', '1234');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_enkripsi`
--
ALTER TABLE `data_enkripsi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_enkripsi`
--
ALTER TABLE `data_enkripsi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
