import Image from 'next/image'
import Link from 'next/link';
import Cookie from 'js-cookie'
import Router from 'next/router';

export default function Navbar() {
    let buttonNavbar;
    let has_token = false;

    function logoutHandle(e){
        e.preventDefault();
        Cookie.remove('kel-10-token')
        Router.push('/login')
    }

    has_token = Cookie.get('kel-10-token') ? true : false
    console.log("has token = "+has_token);
    buttonNavbar = (has_token == true) ? <button><Link href={``} onClick={logoutHandle} className="btn bg-red-400">Keluar</Link></button> : <button><Link href={`/login`} className="btn">Masuk</Link></button>
    return (
    <nav className='w-full'>
        <div className="nav-bg"></div>
        <div className="nav1">
            <Image src="/favicon.jpg" alt="" width={100} height={100} />
            <Link href="/">Kriptor</Link>
        </div>
        <div className="nav2">
            <div className="nav-list">
                <Link href="/enkripsi" className="nav-item">Enkripsi</Link>
                <Link href="/dekripsi" className="nav-item">Dekripsi</Link>
                {/* <Link href="/kelola" className="nav-item">Kelola</Link> */}
            </div>
            <div className="login">
                {buttonNavbar}
            </div>
        </div>
    </nav>
    )

}