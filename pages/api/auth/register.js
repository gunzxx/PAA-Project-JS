const mysql = require('mysql2/promise');

export default async function handler(req, res) {
    const connection = await mysql.createConnection({ host: 'gunzxx.my.id', user: 'gunzxxmy_paa-project', database: 'gunzxxmy_paa-project', password: 'Sandiuno_' });
    // const connection = await mysql.createConnection({ host: 'localhost', user: 'root', database: 'paa_project' });
    let {method,body} =req;
    if(typeof body!=='object'){
        return res.status(400).json({
            status: false,
            method: method,
            message: "Ganti tipe data form menjadi x-www-form-urlencoded"
        })
    }

    switch(method){
        case "POST":
            try{
                const username = body.username
                const nickname = body.nickname
                const password = body.password
                const password_konfirmasi = body.password_konfirmasi

                if (username.length < 1 | nickname.length < 1 | password.length < 1 | password_konfirmasi.length < 1) {
                    return res.status(400).json({
                        status: false,
                        method: method,
                        message: "Data tidak boleh kosong"
                    })
                }

                if (password != password_konfirmasi) {
                    return res.status(400).json({
                        status: false,
                        method: method,
                        message: "Kata sandi tidak sama"
                    })
                }

                let is_user_exist = false
                const [user] = await connection.execute("SELECT * FROM `users` where username = '" + username + "' ");
                if (user.length > 0) {
                    is_user_exist = true;
                    return res.status(400).json({
                        status: false,
                        method: method,
                        message: "Username sudah digunakan"
                    })
                }

                if (is_user_exist == false) {
                    const [user2] = await connection.execute("INSERT INTO users(username,nickname,password) values('" + username + "','" + nickname + "','" + password + "')");
                    return res.status(200).json({
                        status: true,
                        method: method,
                        message: "Registrasi sukses",
                        data: user2,
                    })
                }
            }catch(e){
                console.log(e);
                return res.status(500).json({
                    status: true,
                    method: method,
                    message: "Server erro",
                })
            }

        default:
            return res.status(400).json({
                status: false,
                method: method,
                message: "Your method is not supported"
            })
    }
}