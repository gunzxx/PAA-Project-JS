export async function getServerSideProps(context) {
    const { id } = context.query;

    return {
        props: {
            id,
        },
    };
}

export default function(req, res) {
    return res.json(req.query).status(200)
}
