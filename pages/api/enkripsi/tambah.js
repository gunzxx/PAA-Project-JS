import jwtDecode from 'jwt-decode';

const mysql = require('mysql2/promise');

export default async function handler(req, res) {
    const connection = await mysql.createConnection({ host: 'gunzxx.my.id', user: 'gunzxxmy_paa-project', database: 'gunzxxmy_paa-project', password: 'Sandiuno_' });
    // const connection = await mysql.createConnection({ host: 'localhost', user: 'root', database: 'paa_project' });

    let { method, body } = req;
    // return res.status(200).json(body);

    if (typeof body !== 'object') {
        return res.status(400).json({
            status: false,
            method: method,
            message: "Ganti tipe data form menjadi x-www-form-urlencoded"
        })
    }

    switch (method) {
        case "POST":
            const plaintext = body.plaintext
            const ciphertext = body.ciphertext
            const user_id = body.user_id

            if (plaintext.length < 1 || ciphertext.length < 1 ){
                return res.status(400).json({
                    status: false,
                    method: method,
                    message: "Jangan kosong",
                })
            }

            // return res.json(body).status(200)
            let is_user_exist = false;
            const [user] = await connection.execute('SELECT * FROM `users` where id = '+user_id);
            if(user.length <1){
                return res.status(400).json({
                    status: false,
                    method: method,
                    message: "User not found"
                })
            }else{
                is_user_exist = true;
            }

            if (is_user_exist == true) {
                const [user2] = await connection.execute("INSERT INTO data_enkripsi(plaintext,ciphertext,user_id) values('" + plaintext + "','" + ciphertext + "'," + user_id + ")");
                connection.end();
                return res.status(200).json({
                    status: true,
                    method: method,
                    message: "Berhasil ditambahkan",
                    data: user2,
                })
            }
            connection.end();

        default:
            return res.status(400).json({
                status: false,
                method: method,
                message: "Your method is not supported"
            })
    }
}