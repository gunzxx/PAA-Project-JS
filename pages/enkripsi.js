import Head from 'next/head'
import React, { useState, useRef, useEffect } from 'react'
import { redirect } from 'react-router-dom';
import Navbar from '../components/navbar';
import { useRouter } from 'next/router'
import $ from 'jquery'
import jwtDecode from 'jwt-decode';
import Cookie from 'js-cookie'



async function getEnkripsiData(){
    var Itoken = { id: '1' };
    var tokenId;

    if (typeof window !== 'undefined') {
        const IjwtPaa = Cookie.get('kel-10-token')
        if (IjwtPaa) {
            Itoken = jwtDecode(IjwtPaa)
            tokenId = Itoken.id;
        }
        else {
            return redirect('/login',200)
        }
    }
    
    let dataReturn = {};
    await $.ajax({
        url: '/api/enkripsi/semua',
        method: "POST",
        dataType: 'json',
        data:{
            user_id: tokenId,
        },
        success: (e) => { 
            // console.log(e);
            dataReturn = e;
        },
        error: (e) => { 
            // console.log(e); 
        },
    })
    return dataReturn;
}


export default function Home() {
    const router = useRouter();
    // console.log(router.basePath);
    
    if (typeof (window) !== 'undefined') {
        if (!Cookie.get('kel-10-token')) {
            router.push('/login')
        }
    }

    const [plaintext, setplaintext] = useState("");
    const [ciphertext, setciphertext] = useState("");

    const plaintextInput = useRef(null);
    const ciphertextInput = useRef(null);

    const plaintextInput2 = useRef(null);
    const ciphertextInput2 = useRef(null);
    const idEditDataInput2 = useRef(null);
    const tbody = useRef(null);
    var payload = {id:''};
    var jwtPaa = '';

    if (typeof window !== 'undefined') {
        jwtPaa = Cookie.get('kel-10-token')
        if (jwtPaa) {
            payload = jwtDecode(jwtPaa)
        }
        else {
            router.push('/login')
        }
    }

    var [data_enkripsi, setdata_enkripsi]= useState([])
    async function showEnkripsiData(){
        data_enkripsi = []
        console.log("Show")
        await getEnkripsiData().then(async (e)=>{ 
            tbody.current.innerHTML = '';
            Object.values(e).map((value)=>{
                data_enkripsi.push(value)
            })
            setdata_enkripsi(data_enkripsi);

            data_enkripsi.forEach(async (e)=>{
                // create element
                let tre = document.createElement('tr')
                let td1 = document.createElement('td')
                let td2 = document.createElement('td')
                let td3 = document.createElement('td')
                let div = document.createElement('div')
                // add <div> tailwind class
                div.classList.add('flex')
                div.classList.add('justify-between')
                // set edit button value
                let edit = document.createElement('a')
                edit.setAttribute("class", "edit");
                edit.setAttribute("id", e.id);
                edit.innerHTML = `<div className='h-[5px]'><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6"><path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" /></svg></div>`
                // set hapus button value
                let hapus = document.createElement('a')
                hapus.setAttribute("class", "hapus");
                hapus.setAttribute("id", e.id);
                hapus.innerHTML = `<div className='h-[5px]'><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6"><path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" /></svg></div>`
                // set column value
                td1.innerHTML = e.plaintext
                td2.innerHTML = e.ciphertext
                div.append(hapus)
                div.append(edit)
                td3.append(div)
                // add td to tr
                tre.append(td1);
                tre.append(td2);
                tre.append(td3);
                tbody.current.append(tre)
            })
        })

        document.querySelectorAll('.hapus').forEach((hapus,key,parent)=>{
            hapus.addEventListener('click',(e)=>{
                let lanjut = confirm("Hapus?");
                if(lanjut==true){
                    $.ajax({
                        url: '/api/enkripsi/hapus',
                        method: 'put',
                        dataType: 'json',
                        data: {
                            id: hapus.id
                        },
                        success: (e) => {
                            console.log(e);
                            showEnkripsiData();
                            alert(e.message);
                        },
                        error: (e) => {
                            console.log(e);
                            alert("Error : " + e.message);
                            console.log("NO");
                        },
                    })
                }
            })
        })

        document.querySelectorAll('.edit').forEach((edit,key,parent)=>{
            edit.addEventListener('click',(e)=>{
                plaintextInput2.current.value = ''
                ciphertextInput2.current.value = ''
                idEditDataInput2.current.value = ''
                $.ajax({
                    url: '/api/enkripsi/edit',
                    method: 'put',
                    dataType: 'json',
                    data: {
                        id: edit.id
                    },
                    success: (e) => {
                        plaintextInput2.current.value = e.data.plaintext;
                        ciphertextInput2.current.value = e.data.ciphertext;
                        idEditDataInput2.current.value = e.data.id;
                        console.log(idEditDataInput2.current.value);
                        document.querySelector(".modalEdit").style.display = 'flex';
                        // $(".modalEdit").css('dislay', 'flex')
                    },
                    error: (e) => {
                        console.log(e);
                        alert("Error : "+e.message);
                        console.log("NO");
                    },
                })
            })
        })
    }

    function editDataEnkripsi(){
        let plaintextData2 = plaintextInput2.current.value.trim();
        let ciphertextData2 = ciphertextInput2.current.value.trim();
        let idData2 = idEditDataInput2.current.value.trim();

        let lanjut = false;
        if (plaintextData2.length < 1 || (idData2 == undefined || idData2 == null || idData2 == '')){
            alert("Data kosong, Isi data!");
            lanjut = false;
        }else{
            lanjut = true;
        }

        if(lanjut!=false){
            $.ajax({
                url:'/api/enkripsi/ubah',
                method:'patch',
                dataType:'json',
                data:{
                    plaintext:plaintextData2,
                    ciphertext:ciphertextData2,
                    id:idData2,
                },
                success : (e)=>{
                    console.log(e);
                    showEnkripsiData();
                    alert(e.message);
                },
                error : (e)=>{
                    console.log(e);
                },
            })
            console.log("Edit");
        }
    }

    function hideData(){
        tbody.current.innerHTML = '';
        console.log("Hide");
    }
    function closeModalEdit(){
        document.querySelector(".modalEdit").style.display = 'none'
    }

    const decryptData = ()=>{
        ciphertextInput.current.value = btoa(plaintextInput.current.value)
        setplaintext(plaintextInput.current.value)
        setciphertext(btoa(plaintextInput.current.value))
    }
    const decryptData2 = ()=>{
        ciphertextInput2.current.value = btoa(plaintextInput2.current.value)
        setplaintext(plaintextInput2.current.value)
        setciphertext(btoa(plaintextInput2.current.value))
    }

    const copyText = ()=>{
        navigator.clipboard.writeText(ciphertextInput.current.value);
        alert('Text disalin')
    }

    const clearText = ()=>{
        plaintextInput.current.value =''
        ciphertextInput.current.value =''
    }
    const clearText2 = ()=>{
        plaintextInput2.current.value =''
        ciphertextInput2.current.value =''
    }

    const saveEncrypt = () => {
        const data_req = {
            plaintext: plaintext,
            ciphertext: ciphertext,
            user_id:payload.id
        }
        if(plaintext.length <1 || ciphertext.length < 1){
            return alert("Gagal")
        }
        
        $.ajax({
            url:'/api/enkripsi/tambah',
            method : "POST",
            dataType : 'json',
            data:data_req,
            success:(e)=>{console.log(e);showEnkripsiData();alert(e.message);},
            error: (e) => {console.log(e);alert(e.message);},
        });
    }

    return (
        <>
            <Head>
                <title>Create Next App</title>
                <meta name="description" content="Generated by create next app" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.jpg" />
                <link rel="stylesheet" href="/css/global.css" />
                <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;800;900&display=swap" rel="stylesheet"></link>
                <link rel="stylesheet" href="/css/navbar.css" />
                <link rel="stylesheet" href="/css/login.css" />
            </Head>
            <div className='w-full min-h-[100vh]'>
                <Navbar></Navbar>

                <div className="modalEdit modal-container content-[''] fixed top-[0] z-[5] w-[100vw] h-[100vh] items-center justify-center hidden">
                    <div onClick={closeModalEdit} className="w-[100vw] h-[100vh] backdrop-blur-lg fixed top-0"></div>
                    <div className="p-20 bg-orange-300 shadow-lg shadow-slate-500 rounded-xl z-10">
                        <button className='closeEditModal' onClick={closeModalEdit} style={{ color:'black',width:10+'px',height:10+'px' }}>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                            </svg>
                        </button>
                        <form onSubmit={(e) => { e.preventDefault(); editDataEnkripsi() }}>
                            <div className="form-group">
                                <label htmlFor="plaintext">Masukkan plaintext</label>
                                <input ref={plaintextInput2} maxLength={20} onKeyDown={decryptData2} minLength={3} type="text" name="plaintext" id="plaintext" autoFocus={true} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="ciphertext">ciphertext</label>
                                <input title="Click to copy" style={{ cursor: `pointer` }} onClick={copyText} ref={ciphertextInput2} readOnly onChange={e => { setciphertext(e.target.value); }} minLength={3} required={true} type="ciphertext" name="ciphertext" id="ciphertext" />
                            </div>
                            <input type="hidden" ref={idEditDataInput2} />
                            <div className="w-full form-group login flex items-end justify-end">
                                <button type='reset' onClick={clearText2} className="btn bg-red-400">Hapus</button>
                                <button type='submit' className="btn bg-blue-500">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div className="main-login gap-[30px] justify-between px-[50px] relative top-[150px] w-full">
                    <div className="card-container">
                        <form className="form-container" onSubmit={e => { e.preventDefault(); saveEncrypt() }}>
                            <div className="form-group">
                                <label htmlFor="plaintext">Masukkan plaintext</label>
                                <input ref={plaintextInput} maxLength={20} onChange={decryptData} minLength={3} type="text" name="plaintext" id="plaintext" autoFocus={true} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="ciphertext">ciphertext</label>
                                <input title="Click to copy" style={{ cursor: `pointer` }} onClick={copyText} ref={ciphertextInput} readOnly onChange={e => { setciphertext(e.target.value); }} minLength={3} required={true} type="ciphertext" name="ciphertext" id="ciphertext" />
                            </div>
                            <div className="w-full form-group login flex items-end justify-end">
                                <button type='reset' onClick={clearText} className="btn bg-red-400">Hapus</button>
                                <button type='submit' className="btn bg-blue-500">Simpan</button>
                            </div>
                        </form>
                    </div>
                    <div className="data-container min-h-[100px] bg-purple-400 shadow-lg shadow-slate-400 rounded-xl my-[40px] w-[100%] p-[20px] min-w[40%] flex-col justify-center items-center">
                        <div className="action-btn w-full flex justify-between">
                            <button className='btn mb-8' onClick={showEnkripsiData} type='button'>show data</button>
                            <button className='btn mb-8' onClick={hideData} type='button'>hide data</button>
                        </div>
                        <table>
                            <thead className='bg-pink-400'>
                                <tr>
                                    <td>Plaintext</td>
                                    <td>Ciphertext</td>
                                    <td>Aksi</td>
                                </tr>
                            </thead>
                            <tbody className='w-full' ref={tbody}>
                            </tbody>
                        </table>
                        <ul>
                        </ul>
                    </div>
                </div>
            </div>
        </>
    )
}